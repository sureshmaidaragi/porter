# Demo App
Code is written in Kotlin

### Features
1. Product Listing
2. Product Details and Google Map

### How to generate APK from source code
1. Clone the source code to your local machine
2. Open local code from latest Android Studio
3. Replace google_map_key from string.xml file
4. If you not replace google map key Google map may not work under Delivery details screen
5. Clean and Build code from Android Studio
6. Click Build from top navigation items > Build Bundles()/APK > 
7. Once APK generated you can find in finders


### How to get debug APK
1. There is already generated APK upload in source code named  app-debug.apk. One can download install in any supported android phones
2. If you face any problem email me maidaragi1919@gmail.com




