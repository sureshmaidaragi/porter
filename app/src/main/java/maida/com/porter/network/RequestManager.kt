package maida.com.porter.network

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import java.io.IOException


class RequestManager {


    companion object {

        fun OkHttpGetData(mContext: Context?, urlString: String): String {
            val client = OkHttpClient()
            println("OPERATION  " + urlString!!)

            val request = Request.Builder()
                    .url(urlString)
                    .get()
                    .build()

            try {

                val response = client.newCall(request).execute()

                print("response.body" + response.body())
                print(" response.code" + response.code())
                if (response.code() == 200) {

                    val jsonData = response.body()!!.string()

                    println("___urlString____" + urlString)
                    println("___OkHttpGETjsonData___" + jsonData)
                    return jsonData;
                } else {

                    return response.code().toString()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return ""
        }


    }
}