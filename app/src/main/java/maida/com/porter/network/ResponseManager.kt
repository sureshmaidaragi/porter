package maida.com.porter.network

import maida.com.porter.model.Location
import maida.com.porter.model.ProductModel
import org.json.JSONArray
import org.json.JSONObject

class ResponseManager {

    companion object {
        fun parseProductRespo(response: String): ArrayList<ProductModel> {

            var productList = ArrayList<ProductModel>()
            try {

                val jsonArray = JSONArray(response)

                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {

                        val jsonData = jsonArray.getJSONObject(i)

                        val id: Int = if (jsonData.has("id")) {
                            jsonData.getInt("id")
                        } else {
                            0
                        }
                        val description: String = if (jsonData.has("description")) {
                            jsonData.getString("description")
                        } else {
                            ""
                        }
                        val imageUrl: String = if (jsonData.has("imageUrl")) {
                            jsonData.getString("imageUrl")
                        } else {
                            ""
                        }
                        val locationObj: JSONObject? = if (jsonData.has("location")) {
                            jsonData.getJSONObject("location")
                        } else {
                            null
                        }

                        var location: Location? = null

                        if (locationObj != null) {
                            val lat: Double = if (locationObj.has("lat")) {
                                locationObj.getDouble("lat")
                            } else {
                                0.0
                            }
                            val lng: Double = if (locationObj.has("lng")) {
                                locationObj.getDouble("lng")
                            } else {
                                0.0
                            }
                            val address: String = if (locationObj.has("address")) {
                                locationObj.getString("address")
                            } else {
                                ""
                            }

                            location = Location(lat, lng, address);
                        }

                        val productModel = ProductModel(id, description, imageUrl, location!!)

                        productList.add(productModel)

                    }

                } else {

                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return productList
        }

    }

}