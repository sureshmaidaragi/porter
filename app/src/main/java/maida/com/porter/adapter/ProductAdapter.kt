package maida.com.porter.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import maida.com.porter.ProductDetailsActivity
import maida.com.porter.R
import maida.com.porter.model.ProductModel


class ProductAdapter(val productListL: ArrayList<ProductModel>, val contextL: Context) : RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {

    var productList: MutableList<ProductModel>? = null
    var context: Context? = null

    init {
        productList = productListL
        context = contextL
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.product_row_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return productList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val product = productList?.get(position)

        holder.txt_descr.setText(product?.description)
        Glide.with(context!!).load(product?.imageUrl).into(holder?.img_preview!!)

        holder.card_item.setOnClickListener {

            print("product "+product?.location)
            val bundle = Bundle()
            bundle.putString("desc", product?.description)
            bundle.putString("imageUrl", product?.imageUrl)
            bundle.putInt("id", product?.id!!)
            bundle.putDouble("lat", product?.location?.lat!!)
            bundle.putDouble("long", product?.location?.lng!!)
            bundle.putString("address", product?.location?.address!!)
            context?.startActivity(Intent(context, ProductDetailsActivity::class.java).putExtra("product_data", bundle))

        }

    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txt_descr: TextView
        var img_preview: ImageView
        var card_item: CardView

        init {
            img_preview = view.findViewById(R.id.img_preview)
            txt_descr = view.findViewById(R.id.txt_descr)
            card_item = view.findViewById(R.id.card_item)
        }
    }

}