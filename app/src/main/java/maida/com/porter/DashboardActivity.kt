package maida.com.porter

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.dashboard_activity.*
import maida.com.porter.adapter.ProductAdapter
import maida.com.porter.model.ProductModel
import maida.com.porter.network.ResponseManager
import maida.com.porter.threads.GetProductThread

class DashboardActivity : BaseActivity() {
    var TAG: String = "DashboardActivity";
    var productAdapter: ProductAdapter? = null
    val llm = LinearLayoutManager(this)

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {

        override fun handleMessage(msg: Message?) {

            if (msg?.what == GetProductThread.GETPRODUCTTHREAD_SUCCESS) {
                product_progress?.visibility = View.GONE

                updateUI(msg.obj.toString())


            } else if (msg?.what == GetProductThread.GETPRODUCTTHREAD_FAIL) {
                product_progress?.visibility = View.GONE

                BaseActivity.make_toast(applicationContext, resources.getString(R.string.pleaseTryAgain))
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard_activity)

        if (isInternetAvailable(applicationContext)) {
            product_progress?.visibility = View.VISIBLE
            GetProductThread(applicationContext, mHandler).start()
        } else {
            product_progress?.visibility = View.GONE

        }

    }

    fun updateUI(response: String) {

        updateAdapter(ResponseManager.parseProductRespo(response))

    }

    fun updateAdapter(productList: ArrayList<ProductModel>) {

        llm.orientation = LinearLayoutManager.VERTICAL
        product_recyclerview?.setLayoutManager(llm)
        productAdapter = ProductAdapter(productList, applicationContext)
        product_recyclerview?.adapter = productAdapter

    }

}
