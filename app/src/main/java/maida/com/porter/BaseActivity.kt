package maida.com.porter

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import java.util.*


open class BaseActivity : AppCompatActivity() {

    companion object {


        fun make_toast(mContext: Context, mContent_msg: String) {

            Toast.makeText(mContext, mContent_msg, Toast.LENGTH_SHORT).show()
        }

        fun make_long_toast(mContext: Context, mContent_msg: String) {

            Toast.makeText(mContext, mContent_msg, Toast.LENGTH_LONG).show()
        }


        /**
         * Checks for available internet connection
         */
        fun isInternetAvailable(ctx: Context): Boolean {
            val result = false
            val conMgr = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return if (conMgr.activeNetworkInfo != null) {
                conMgr.activeNetworkInfo.isConnected
            } else {

                BaseActivity.make_toast(ctx, ctx.resources.getString(R.string.no_internet))
                result
            }
        }

    }

    fun requestPermission(activity: Activity, requestCode: Int, vararg permissions: String): Boolean {
        var granted = true
        val permissionsNeeded = ArrayList<String>()

        for (s in permissions) {
            val permissionCheck = ContextCompat.checkSelfPermission(activity, s)
            val hasPermission = permissionCheck == PackageManager.PERMISSION_GRANTED
            granted = granted and hasPermission
            if (!hasPermission) {
                permissionsNeeded.add(s)
            }
        }

        if (granted) {
            return true
        } else {
            ActivityCompat.requestPermissions(activity,
                    permissionsNeeded.toTypedArray(),
                    requestCode)
            return false
        }
    }


    fun checkWriteExternalPermission(permission: String): Boolean {
        // val permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        val res = checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }
}