package maida.com.porter

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.product_details_activity.*


class ProductDetailsActivity() : BaseActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private var mLat: Double? = 0.0
    private var mLong: Double? = 0.0
    private var mAddress: String? = null
    private var mDesc: String? = null
    private val LOCATION_REQUEST_CODE = 12

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap
        mMap?.addMarker(MarkerOptions().position(LatLng(mLat!!, mLong!!)).title(mAddress!!))
        mMap?.setMyLocationEnabled(true)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(LatLng(mLat!!, mLong!!)))
        mMap?.animateCamera(CameraUpdateFactory.zoomTo(19.0f))
        mMap?.getUiSettings()?.isZoomControlsEnabled = true

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_details_activity)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()!!.setTitle("Delivery Details")

        initView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun initView() {
        try {
            val bundle = intent.extras.getBundle("product_data")
            Glide.with(applicationContext).load(bundle.getString("imageUrl")).into(img_preview)
            txt_descr.setText(bundle.getString("desc"))
            mLat = bundle.getDouble("lat")
            mLong = bundle.getDouble("long")
            mAddress = bundle.getString("address")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (checkWriteExternalPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            loadGmap()

        } else {
            if (requestPermission(
                            this@ProductDetailsActivity,
                            LOCATION_REQUEST_CODE,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                loadGmap()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LOCATION_REQUEST_CODE -> if (checkWriteExternalPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)) loadGmap()

        }
    }

    fun loadGmap() {
        val mapFragment = SupportMapFragment.newInstance()
        mapFragment.getMapAsync(this)

        supportFragmentManager
                .beginTransaction()
                .add(fragment_gmap.getId(), mapFragment)
                .commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }
}