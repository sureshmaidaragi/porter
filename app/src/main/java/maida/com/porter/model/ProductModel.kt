package maida.com.porter.model

data class ProductModel(
        val id: Int,
        val description: String,
        val imageUrl: String,
        val location: Location
)