package maida.com.porter.threads

import android.content.Context
import android.os.Handler
import android.os.Message
import maida.com.porter.network.RequestManager

class GetProductThread() : Thread() {


    var mHandler: Handler? = null
    private var mContext: Context? = null

    companion object {
        val GETPRODUCTTHREAD_SUCCESS = 1;
        val GETPRODUCTTHREAD_FAIL = -1;
        val TAG = "GETPRODUCTTHREAD"
        var OPERATION: String = "https://mock-api-mobile.dev.lalamove.com/deliveries"

    }

    constructor(mContext: Context, mHandler: Handler) : this() {

        this.mContext = mContext
        this.mHandler = mHandler


    }

    override fun run() {

        val respString = RequestManager.OkHttpGetData(mContext, OPERATION)

        if (respString != null) {
            sendSuccess(respString)
        } else {
            sendFailure()
        }
    }

    fun sendSuccess(respString: String) {

        var msg: Message = Message();
        msg.what = GETPRODUCTTHREAD_SUCCESS;
        msg.obj = respString;
        mHandler!!.sendMessage(msg);
    }

    fun sendFailure() {

        mHandler!!.sendEmptyMessage(GETPRODUCTTHREAD_FAIL);
    }
}